Jax-rs kata
===========

 The purpose of this kata is to learn the basics of jax-rs.
 The rest of the document is in French.
 
Structure du projet
-------------------
 
 Le projet est un projet maven/java qui package un war.
 
 Lancez la commande 
 
    mvn install
 
 pour build le projet et lancer les tests.

 Dans le projet vous trouverez un service `HelloWorld` avec un test. Le test utilise le wrapper `SimpleHttpServer` pour démarrer un server HTTP.
 

Kata
----
 
 Utilisez le [memo jax-rs ](jax-rs-memo.md) pendant la session. 
 
### Instructions  
 
 Le but est de créer un annuaire de candidat et un service de planification d'entretien.
 
 Dans notre exercices un candidat (Applicant) a un nom, un prénom et une adresse mail

 Respectez les bonnes pratiques pendant l'exercices : codes HTTP, noms des endpoints etc...
 
#### Partie 1 
 
 1. Faites un service qui permet de retourner la liste de tout les candidats
    - le endpoint sera un GET et produira du XML et du JSON
    - vous pouvez utiliser une liste statique pour vous aider pour la suite
 2. Faites un service qui permet d'ajouter un candidat
   - le endpoint sera un POST qui consomera du XML et du JSON
   - la réponse doit etre l URL (l'id) de l'element
 3. Faites un service qui permet de retourner les informations d'un seul candidat à partir de son id (en @PathParam)
   - le endpoint sera un GET et produira du XML et du JSON
 4. Faites un service qui permet de mettre à jour les informations d'un candidat à partir de son id (en @PathParam)
   - le endpoint sera un PUT et consomera du XML et du JSON
 5. Faites un service qui permet de supprimer un candidat à partir de son id (en @PathParam)
   - le endpoint sera un DELETE
 6. Rajoutez au service qui liste les candidats la posibilité de filtrer sur le type de profile :
   - ajoutez un champ "type" à l'object applicant (valeur possible de l'enum : "Junior", "Middle", "Senior", "Legend")
   - ajoutez au service le type de profile en @QueryParam
   - si le QueryParam est null alors retourner la liste complete
   - si le QueryParam est une valeur valide retourner que les candidats qui correspondent

#### Partie 2 (libre)

 - Faites les services nécessaires pour planifier des entretiens entre candidat et RH 


 

