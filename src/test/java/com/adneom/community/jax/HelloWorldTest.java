package com.adneom.community.jax;

import com.adneom.community.jax.server.SimpleHttpServer;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Collections;

import static javax.ws.rs.core.MediaType.APPLICATION_XML_TYPE;
import static org.junit.Assert.assertEquals;


public class HelloWorldTest {

    static final int SERVER_PORT = 7000;
    static final String ROOT_ENDPOINT = "http://localhost:" + SERVER_PORT;
    static SimpleHttpServer server = new SimpleHttpServer(Collections.singleton(HelloWorld.class), SERVER_PORT);

    /*
        Start the server wiht the HelloWorld service.
     */
    public static void main(String[] args) throws IOException {
        startServer();
    }

    @BeforeClass
    public static void startServer() throws IOException {
        server.start();
    }

    @AfterClass
    public static void stopServer() {
        server.stop();
    }

    @Test
    public void sayHello_response_should_containt_hello_wolrd() {

        Client client = ClientBuilder.newClient();
        Response response = client.target(ROOT_ENDPOINT + "/hello").request("text/plain").get();

        assertEquals(200, response.getStatus());
        assertEquals(HelloWorld.HELLO_WORLD_MESSAGE, response.readEntity(String.class));
    }

    @Test
    public void sayHelloTo_response_should_containt_hello_bob_when_name_is_bob() {
        Client client = ClientBuilder.newClient();
        Response response = client.target(ROOT_ENDPOINT + "/hello/bob").request("text/plain").get();

        assertEquals(200, response.getStatus());
        assertEquals("hello bob", response.readEntity(String.class));
    }

    @Test
    public void sayHelloToApplicant_response_should_containt_applicant() {
        Client client = ClientBuilder.newClient();
        Response response = client.target(ROOT_ENDPOINT + "/hi/jean?lastName=Dupond").request().get();

        assertEquals(200, response.getStatus());
        Applicant expectedApplicant = new Applicant("jean", "Dupond", "jean.Dupond@mail.com");
        assertEquals(expectedApplicant, response.readEntity(Applicant.class));
    }

    @Test
    public void postApplicant_response_should_containt_applicant() {

        Applicant applicant = new Applicant("jean", "Dupond", "jean.Dupond@mail.com");
        Client client = ClientBuilder.newClient();
        Response response = client.target(ROOT_ENDPOINT + "/hello")
                .request()
                .post(Entity.entity(applicant, APPLICATION_XML_TYPE));

        assertEquals(200, response.getStatus());
        assertEquals("hello "+applicant.toString(), response.readEntity(String.class));
    }
}