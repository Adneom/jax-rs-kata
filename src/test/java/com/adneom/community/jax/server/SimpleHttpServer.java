package com.adneom.community.jax.server;

import com.sun.net.httpserver.HttpServer;
import org.jboss.resteasy.plugins.server.sun.http.HttpContextBuilder;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.Collection;

/**
 * HttpServer wrapper used for tests.
 */
public class SimpleHttpServer {

    private HttpServer server;

    public SimpleHttpServer(Collection<? extends Class> classes, int port) {
        tryToCreateHttpServer(port);
        bindHttpContextToServer(classes);
    }

    private void tryToCreateHttpServer(int port) {
        int backlogDefaultValue = 0;
        try {
            server = HttpServer.create(new InetSocketAddress(port), backlogDefaultValue);
        } catch (IOException e) {
            throw new RuntimeException("Fail to start HTTP server on port  : " + port, e);
        }
    }

    private void bindHttpContextToServer(Collection<? extends Class> classes) {
        HttpContextBuilder contextBuilder = new HttpContextBuilder();
        contextBuilder.getDeployment()
                .getActualResourceClasses()
                .addAll(classes);
        contextBuilder.bind(server);
    }

    public void start() {
        server.start();
    }

    public void stop() {
        int delay = 0;
        server.stop(delay);
    }

}
