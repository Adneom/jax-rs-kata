package com.adneom.community.jax;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;


@Path("/")
public class HelloWorld {

    public static final String HELLO_WORLD_MESSAGE = "hello world !";

    @Path("/hello")
    @GET
    public Response sayHello() {
        return Response.ok(HELLO_WORLD_MESSAGE).build();
    }

    @Path("/hello/{name}")
    @GET
    public Response sayHelloTo(@PathParam(value = "name") String name) {
        return Response.ok("hello " + name).build();
    }

    @Path("/hi/{firstName}")
    @GET
    @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    public Applicant sayHelloToApplicant(@PathParam(value = "firstName") String firstName, @QueryParam("lastName") String lastName) {
        return new Applicant(firstName, lastName, firstName + "." + lastName + "@mail.com");
    }

    @Path("/hello")
    @POST
    @Consumes(value = {MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
    @Produces(value = {MediaType.TEXT_PLAIN})
    public Response postApplicant(Applicant applicant) {
        return Response.ok("hello " + applicant.toString())
                .build();
    }

}
